﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YoutubeDLFrontEnd
{
    public class Program
    {
        static void Main(string[] args)
        {
            bool exitProgram = false;
            string option = "";

            bool isSplash = true;

            Console.WriteLine("------------------------------------");
            Console.WriteLine("Welcome to Youtube-DL Frontend");
            Console.WriteLine("------------------------------------");
            Console.WriteLine();


            while (!exitProgram)
            {

                if (!isSplash)
                {
                    Console.Clear();
                }

                Console.WriteLine("Option 1: Download video from URL ");
                Console.WriteLine("Option 2: Download videos from videos.txt list file ");
                Console.WriteLine("Option 3: Exit ");
                Console.Write("Choose your option: ");

                option = Console.ReadLine();
                switch (option)
                {
                    case "1":

                        Console.Write("Enter the URL of the video to download: ");
                        string url = Console.ReadLine();

                        var t = Task.Run(() => DownloadVideoByUrl(url));
                        t.Wait();

                        Console.WriteLine("Video(s) have been downloaded. Press any key to continue");
                        Console.ReadLine();

                        break;
                    case "2":

                        var t2 = Task.Run(() => DownloadByFileList());
                        t2.Wait();

                        Console.WriteLine("Video(s) have been downloaded. Press any key to continue");
                        Console.ReadLine();

                        break;
                    case "3":

                    default:
                        exitProgram = true;
                        break;
                }

                isSplash = false;
            }




        }


        public static async Task<bool> DownloadByFileList()
        {
            var exists = File.Exists("videos.txt");

            List<string> lines = File.ReadAllLines("videos.txt", Encoding.UTF8).OfType<string>().ToList();


            var tasks = lines.Select(async URL =>
            {

                await Task.Run(() => {

                    string folderLocation = GetFolderLocation();

                    var tcs = new TaskCompletionSource<bool>();
                    Process process = new Process();
                    ProcessStartInfo info = new ProcessStartInfo("youtube-dl.exe", $" -i -o {folderLocation} \"{URL}\"");

                    process.EnableRaisingEvents = true;

                    process.StartInfo = info;

                    process.Exited += (sender, args) =>
                    {
                        tcs.SetResult(true);
                        process.Dispose();
                    };

                    process.Start();

                    return tcs.Task;
                });

            });

            await Task.WhenAll(tasks);

            return true;
        }



        private static string GetFolderLocation()
        {
            string folderName = "";

            if (ReadDirectoryLocationFromConfiguration() != "")
                folderName = ReadDirectoryLocationFromConfiguration();
            else
                folderName = Directory.GetCurrentDirectory() + "/videos/";

            // Check if directory exists, if not create it
            if (!Directory.Exists(folderName))
            {
                Directory.CreateDirectory(folderName);
            }

            return $@"""{folderName}%(playlist)s/%(chapter_number)s - %(chapter)s/%(title)s.%(ext)s""";
        }


        public static Task<bool> DownloadVideoByUrl(string url)
        {
            string folderLocation = "";
            folderLocation = GetFolderLocation();

            var tcs = new TaskCompletionSource<bool>();

            Process process = new Process();
            ProcessStartInfo info = new ProcessStartInfo("youtube-dl.exe", $" -i -o {folderLocation} \"{url}\"");

            process.EnableRaisingEvents = true;

            process.StartInfo = info;

            process.Exited += (sender, args) =>
            {
                tcs.SetResult(true);
                process.Dispose();
            };

            process.Start();

            return tcs.Task;
        }


        public static string ReadDirectoryLocationFromConfiguration()
        {
            Configuration configuration = null;

            using (StreamReader r = new StreamReader("appsettings.json"))
            {
                string json = r.ReadToEnd();
                configuration = JsonConvert.DeserializeObject<Configuration>(json);
            }

            return configuration.Directory;
        }

        public class Configuration
        {
            public string Directory { get; set; }
        }


    }
}
